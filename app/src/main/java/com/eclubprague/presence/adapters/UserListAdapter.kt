package com.eclubprague.presence.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eclubprague.presence.R
import com.eclubprague.presence.model.User
import com.eclubprague.presence.utils.DateUtils
import kotlinx.android.synthetic.main.item_user.view.*

class UserListAdapter(val users: MutableList<User>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder(context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: User) = with(itemView) {
            tv_user_name.text = user.name
            tv_timestamp.text = DateUtils.getTimeAgo(context, user.timestamp)
            tv_place.text = user.place
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            ViewHolder(parent.context, LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_user, parent, false))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        holder.bind(users[position])
    }

    override fun getItemCount(): Int = users.size

}