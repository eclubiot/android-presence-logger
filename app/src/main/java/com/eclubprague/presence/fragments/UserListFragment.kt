package com.eclubprague.presence.fragments

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.TimeoutError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.eclubprague.presence.R
import com.eclubprague.presence.adapters.UserListAdapter
import com.eclubprague.presence.getRequestUrl
import com.eclubprague.presence.model.User
import kotlinx.android.synthetic.main.fragment_user_list.*
import org.json.JSONArray
import org.json.JSONObject

class UserListFragment : Fragment() {

    companion object {
        val TAG = UserListFragment::class.simpleName
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_user_list, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe_refresh.setOnRefreshListener { retrievePresentUsers() }

        rv_user_list.setHasFixedSize(true)
        rv_user_list.layoutManager = LinearLayoutManager(activity)
        rv_user_list.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))

        retrievePresentUsers()
    }

    fun retrievePresentUsers() {
        val users = ArrayList<User>()

        val request = StringRequest(Request.Method.GET, getRequestUrl(), {
            val jsonUsers = JSONArray(it)
            (0..jsonUsers.length() - 1)
                    .map { jsonUsers[it] as JSONObject }
                    .mapTo(users) { User.fromJson(it) }

            rv_user_list.adapter = UserListAdapter(users)

            setLoading(false)
        }, {
            if (it is NoConnectionError || it is TimeoutError) {
                Toast.makeText(activity, "Timeout", Toast.LENGTH_LONG).show()
                swipe_refresh.isRefreshing = false
            }
        })

        setLoading(true)

        Volley.newRequestQueue(activity).add(request)
    }

    fun setLoading(loading: Boolean) {
        swipe_refresh.isRefreshing = loading
    }

}