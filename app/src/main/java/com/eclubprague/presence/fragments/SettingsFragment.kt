package com.eclubprague.presence.fragments

import android.os.Bundle
import android.preference.EditTextPreference
import android.preference.PreferenceFragment
import com.eclubprague.presence.BuildConfig
import com.eclubprague.presence.R

class SettingsFragment : PreferenceFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences)

        val textPrefs = arrayOf(
                findPreference(getString(R.string.key_user_name)) as EditTextPreference?
        )

        for (pref in textPrefs) {
            if (pref != null) {
                pref.summary = pref.text
                pref.setOnPreferenceChangeListener { _, newValue ->
                    pref.summary = newValue as String
                    true
                }
            }
        }

        setVersion()
    }

    fun setVersion() {
        val pref = findPreference(getString(R.string.key_app_version))
        pref.summary = BuildConfig.VERSION_NAME
    }

}