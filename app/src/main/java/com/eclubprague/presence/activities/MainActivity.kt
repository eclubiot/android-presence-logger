package com.eclubprague.presence.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.support.annotation.RequiresPermission
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.eclubprague.presence.R
import com.eclubprague.presence.fragments.UserListFragment
import com.eclubprague.presence.getDefaultName
import com.eclubprague.presence.utils.PreferenceHandler

class MainActivity : AppCompatActivity() {

    private val PERMISSION_REQUEST_CONTACTS = 0

    private var explanationDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO: handle orientation changes

        // Handle contacts permission
        val prefs = PreferenceHandler(this)
        if (savedInstanceState == null && !prefs.contains(R.string.key_user_name)) {
            // Ask for contacts permission and if granted, save profile name
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
                    showPermissionExplanation()
                } else requestContactsPermission()
            }
        }

        var fragment = fragmentManager.findFragmentByTag(UserListFragment.TAG)
        if (fragment == null) fragment = UserListFragment()
        fragmentManager.beginTransaction()
                .replace(android.R.id.content, fragment, UserListFragment.TAG)
                .commit()
    }

    override fun onStop() {
        super.onStop()
        if (explanationDialog != null && explanationDialog!!.isShowing) explanationDialog!!.dismiss()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CONTACTS -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Get name from profile and save it to preferences
                    val name = retrieveProfileName()
                    PreferenceHandler(this).putString(R.string.key_user_name, name)
                    Toast.makeText(this, getString(R.string.msg_user_name_set_to, name), Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, R.string.msg_could_not_retrieve_name, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun showPermissionExplanation() {
        val dialog = AlertDialog.Builder(this).create()

        explanationDialog = dialog

        dialog.setTitle(R.string.title_dialog_contacts_permission_explanation)
        dialog.setMessage(getString(R.string.msg_dialog_contacts_permission_explanation))

        dialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.action_ok)) {
            _, _ ->
            requestContactsPermission()
        }

        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.action_do_not_show_again)) {
            _, _ ->
            PreferenceHandler(this).putString(R.string.key_user_name, getDefaultName(this))
        }

        dialog.setOnDismissListener {
            PreferenceHandler(this).putString(R.string.key_user_name, getDefaultName(this))
        }

        dialog.show()
    }

    fun requestContactsPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSION_REQUEST_CONTACTS)
    }

    @RequiresPermission(value = Manifest.permission.READ_CONTACTS)
    fun retrieveProfileName(): String {
        val c: Cursor = applicationContext.contentResolver
                .query(ContactsContract.Profile.CONTENT_URI, null, null, null, null)
        val count = c.count
        c.moveToFirst()

        val name = if (count == 1 && c.position == 0) {
            c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
        } else getDefaultName(this)

        c.close()

        return name
    }

}