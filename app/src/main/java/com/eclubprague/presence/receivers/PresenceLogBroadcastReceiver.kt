package com.eclubprague.presence.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.PowerManager
import android.util.Base64
import android.util.Log
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.eclubprague.presence.*
import com.eclubprague.presence.utils.PreferenceHandler
import org.json.JSONObject
import java.util.*

class PresenceLogBroadcastReceiver : BroadcastReceiver() {

    companion object {
        val TAG = PresenceLogBroadcastReceiver::class.simpleName

        const val TAG_WAKE_LOCK = "PresenceWakeLock"
        const val TIMEOUT_WAKE_LOCK = (5 * 1000).toLong()
    }

    var wakeLock: PowerManager.WakeLock? = null

    override fun onReceive(context: Context, intent: Intent) {
        val bssid = intent.getStringExtra(WifiReceiver.EXTRA_BSSID)
        acquireWakeLock(context)
        sendRequest(context, getUUID(context), bssid)
    }

    fun acquireWakeLock(context: Context) {
        val powerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        if (wakeLock == null) {
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG_WAKE_LOCK)
        }
        wakeLock?.acquire(TIMEOUT_WAKE_LOCK)
    }

    fun releaseWakeLock() {
        if (wakeLock?.isHeld ?: false) {
            wakeLock?.release()
        }
    }

    fun sendRequest(ctx: Context, uuid: String, bssid: String?) {
        val name = PreferenceHandler(ctx).getString(R.string.key_user_name, getDefaultName(ctx))
        val url = getRequestUrl()

        val nameJson = JSONObject()
                .put(KEY_JSON_NAME, name)
                .put(KEY_JSON_UUID, uuid)
                .put(KEY_JSON_BSSID, bssid)

        val request = object : JsonObjectRequest(Request.Method.POST, url, nameJson, {
            response ->
            Log.d(TAG, response.toString())
            releaseWakeLock()
        }, {
            error ->
            Log.d(TAG, error.toString())
            releaseWakeLock()
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val credentials = "root:$API_KEY"
                val auth = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
                val headers = mutableMapOf(
                        "Content-Type" to "application/json",
                        "Authorization" to auth
                )
                return headers
            }
        }

        Volley.newRequestQueue(ctx).add(request)
    }

    fun getUUID(ctx: Context): String {
        val prefs = PreferenceHandler(ctx)

        // If UUID was not set before, generate new and save it
        val uuid: String = prefs.getString(R.string.key_uuid, UUID.randomUUID().toString())

        if (!prefs.contains(R.string.key_uuid)) {
            prefs.putString(R.string.key_uuid, uuid)
        }

        return uuid
    }

}