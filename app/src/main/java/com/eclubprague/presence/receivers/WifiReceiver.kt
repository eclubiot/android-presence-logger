package com.eclubprague.presence.receivers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.util.Log
import com.eclubprague.presence.ALARM_INTERVAL_MIN
import com.eclubprague.presence.TURRIS_BSSID

class WifiReceiver : BroadcastReceiver() {

    companion object {
        val TAG = WifiReceiver::class.simpleName

        const val EXTRA_BSSID = "bssid"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null && intent?.action == WifiManager.NETWORK_STATE_CHANGED_ACTION) {
            val networkInfo = intent.getParcelableExtra<NetworkInfo>(WifiManager.EXTRA_NETWORK_INFO)
            val alarmMgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?

            /* If wifi's new state is connected to Turris, set an alarm which launches service
               which reports to server */
            if (networkInfo?.state == NetworkInfo.State.CONNECTED) {
                val bssid: String? = intent.getStringExtra(WifiManager.EXTRA_BSSID)
                if (bssid.equals(TURRIS_BSSID, ignoreCase = true)) {
                    val pendingIntent = makePendingIntent(context, bssid)

                    Log.d(TAG, "Connected to Turris -> setting alarm")
                    alarmMgr?.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                            1000 * 60 * ALARM_INTERVAL_MIN, pendingIntent)
                }
            } else { // If not connected to Turris, cancel alarm
                alarmMgr?.cancel(makePendingIntent(context, null))
                Log.d(TAG, "Canceling alarm")
            }
        }
    }

    fun makePendingIntent(context: Context, bssid: String?): PendingIntent {
        val intent = Intent(context, PresenceLogBroadcastReceiver::class.java)
        if (bssid != null) intent.putExtra(EXTRA_BSSID, bssid)
        return PendingIntent.getBroadcast(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT)
    }

}