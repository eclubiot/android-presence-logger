package com.eclubprague.presence.model

import com.eclubprague.presence.KEY_JSON_NAME
import com.eclubprague.presence.KEY_JSON_PLACE
import com.eclubprague.presence.KEY_JSON_TIMESTAMP
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

data class User(val name: String, val timestamp: Date, val place: String) {

    companion object {
        fun fromJson(json: JSONObject): User {
            val name = json.getString(KEY_JSON_NAME)
            val time = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ", Locale.getDefault())
                    .parse(json.getString(KEY_JSON_TIMESTAMP))
            val place = json.getString(KEY_JSON_PLACE)
            return User(name, time, place)
        }
    }

}