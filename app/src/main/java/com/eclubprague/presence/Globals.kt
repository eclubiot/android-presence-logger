package com.eclubprague.presence

import android.content.Context
import java.net.URL

const val KEY_JSON_NAME = "fullname"
const val KEY_JSON_UUID = "uuid"
const val KEY_JSON_BSSID = "bssid"
const val KEY_JSON_TIMESTAMP = "timestamp"
const val KEY_JSON_PLACE = "place"

const val TURRIS_BSSID = "bc:30:7d:92:8e:c7"
const val ALARM_INTERVAL_MIN = 5L

const val PROTOCOL = "https"
const val HOST = "presence-logger.herokuapp.com"
const val API_PATH = "/presence-logger/api/v1.0/present-users"
const val API_KEY = BuildConfig.API_KEY

fun getRequestUrl(): String {
    return URL(PROTOCOL, HOST, API_PATH).toString()
}

fun getDefaultName(context: Context): String = context.getString(android.R.string.unknownName)