package com.eclubprague.presence.utils

import android.content.Context
import com.eclubprague.presence.R
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun getTimeAgo(context: Context, timestamp: Date): String {
        val time = timestamp.time
        val now = System.currentTimeMillis()
        val diff = now - time

        val format = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
        val timeStr = " (" + format.format(timestamp) + ")"

        if (diff < 60000) return context.getString(R.string.time_just_now) + timeStr

        return android.text.format.DateUtils.getRelativeTimeSpanString(time).toString() + timeStr
    }

}