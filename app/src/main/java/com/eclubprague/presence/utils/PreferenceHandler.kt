package com.eclubprague.presence.utils

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.preference.PreferenceManager
import android.support.annotation.StringRes

class PreferenceHandler(context: Context) {

    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val res: Resources = context.resources

    private fun key(resourceId: Int) = res.getString(resourceId)

    fun contains(@StringRes resourceId: Int) = prefs.contains(key(resourceId))

    fun getString(@StringRes resourceId: Int, defaultValue: String): String =
            prefs.getString(key(resourceId), defaultValue)

    fun putString(@StringRes resourceId: Int, value: String) {
        prefs.edit().putString(key(resourceId), value).apply()
    }

    fun getStringSet(@StringRes resourceId: Int, defaultValue: Set<String>): Set<String> =
            prefs.getStringSet(key(resourceId), defaultValue)

    fun putStringSet(@StringRes resourceId: Int, value: Set<String>) {
        prefs.edit().putStringSet(key(resourceId), value).apply()
    }

    fun getBoolean(@StringRes resourceId: Int, defaultValue: Boolean): Boolean =
            prefs.getBoolean(key(resourceId), defaultValue)

    fun putBoolean(@StringRes resourceId: Int, value: Boolean) {
        prefs.edit().putBoolean(key(resourceId), value).apply()
    }

    fun getInt(@StringRes resourceId: Int, defaultValue: Int): Int =
            prefs.getInt(key(resourceId), defaultValue)

    fun putInt(@StringRes resourceId: Int, value: Int) {
        prefs.edit().putInt(key(resourceId), value).apply()
    }

    fun getLong(@StringRes resourceId: Int, defaultValue: Long): Long =
            prefs.getLong(key(resourceId), defaultValue)

    fun putLong(@StringRes resourceId: Int, value: Long) {
        prefs.edit().putLong(key(resourceId), value).apply()
    }

    fun getFloat(@StringRes resourceId: Int, defaultValue: Float): Float =
            prefs.getFloat(key(resourceId), defaultValue)

    fun putFloat(@StringRes resourceId: Int, value: Float) {
        prefs.edit().putFloat(key(resourceId), value).apply()
    }

}